<?php

function solution(array $a)
{
    $count = count($a);
    if ($count > 2) {

        sort($a);

        $maxI = $count - 2;
        for ($i = 1; $i <= $maxI; $i++) {
            if (($a[$i - 1] + $a[$i]) > $a[$i + 1]) {
                return 1;
            }
        }
    }

    return 0;
}
