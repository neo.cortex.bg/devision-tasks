<?php

function solution($x, array $a)
{
    $positions = [];
    foreach ($a as $second => $position) {
        if (!isset($positions[$position])) {
            $positions[$position] = $second;
        }
    }

    $maxPosition = count($positions);

    if ($maxPosition < $x) {
        return -1;
    }

    $minSecond = 0;

    for ($i = $x; $i > 0; $i--) {
        if ($positions[$i] > $minSecond) {
            $minSecond = $positions[$i];
        }
    }

    return $minSecond;
}
