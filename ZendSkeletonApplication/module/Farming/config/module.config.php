<?php

namespace Farming;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'parcel' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/parcel[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z]{1,1}[a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ParcelController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'tractor' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/tractor[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\TractorController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'api_tractor' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/api/tractors/[:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\TractorRestController::class,
                    ],
                ],
            ],
            'api_parcel' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/api/parcels/[:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ParcelRestController::class,
                    ],
                ],
            ],
            'api_parcel_treatment' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/api/parcels-treatments/[:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ParcelTreatmentRestController::class,
                    ],
                ],
            ],
            'parcel_treatment' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/treatment[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ParcelTreatmentController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'farming' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
