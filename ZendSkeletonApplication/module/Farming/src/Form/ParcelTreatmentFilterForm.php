<?php

namespace Farming\Form;

use Farming\Model\ParcelTable;
use Farming\Model\TractorTable;
use Zend\Form\Form;
use Farming\Model\ParcelTreatmentTable;
use Zend\Form\Element\Select;
use Zend\Form\Element\Date;

class ParcelTreatmentFilterForm extends Form
{
    public function __construct($name = null, ParcelTable $parcelTable, TractorTable $tractorTable)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('parcel');

        $parcels = $parcelTable->fetchAll();
        $parcelOptions = [];
        foreach ($parcels as $parcel) {
            $parcelOptions[$parcel->id] = $parcel->name;
        }

        $tractors = $tractorTable->fetchAll();
        $tractorOptions = [];
        foreach ($tractors as $tractor) {
            $tractorOptions[$tractor->id] = $tractor->name;
        }

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'parcel_id',
            'type' => Select::class,
            'options' => [
                'label' => 'Parcel',
                'empty_option' => 'Please choose a parcel',
                'value_options' => $parcelOptions,
            ],
        ]);
        $this->add([
            'name' => 'tractor_id',
            'type' => Select::class,
            'options' => [
                'label' => 'Tractor',
                'empty_option' => 'Please choose a tractor',
                'value_options' => $tractorOptions,
            ],
        ]);
        $this->add([
            'name' => 'culture',
            'type' => 'text',
            'options' => [
                'label' => 'Culture',
            ],
        ]);
        $this->add([
            'name' => 'date',
            'type' => Date::class,
            'options' => [
                'label' => 'Date',
                'format' => 'Y-m-d',
            ],
            'attributes' => [
                'min' => '2012-01-01',
                'max' => '2020-01-01',
                'step' => '1', // days; default step interval is 1 day
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Filter',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}