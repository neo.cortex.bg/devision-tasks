<?php

namespace Farming\Form;

use Zend\Form\Form;

class ParcelForm extends Form
{
    public function __construct($name = null)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('parcel');

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Name',
            ],
        ]);
        $this->add([
            'name' => 'culture',
            'type' => 'text',
            'options' => [
                'label' => 'Culture',
            ],
        ]);
        $this->add([
            'name' => 'area',
            'type' => 'text',
            'options' => [
                'label' => 'Area',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}