<?php

namespace Farming\Controller;

use Farming\Form\ParcelTreatmentForm;
use Farming\Model\ParcelTable;
use Farming\Model\ParcelTreatment;
use Farming\Model\ParcelTreatmentTable;
use Farming\Model\TractorTable;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\ServiceManager\ServiceManager;

class ParcelTreatmentRestController extends AbstractRestfulController
{
    private $table;
    private $container;

    public function __construct(ServiceManager $container, ParcelTreatmentTable $table)
    {
        $this->container = $container;
        $this->table = $table;
    }

    public function getList()
    {
        return new JsonModel(
            $this->table->fetchAll()->toArray()
        );
    }

    public function get($id)
    {
        return new JsonModel(
            $this->table->getParcelTreatment($id)->getArrayCopy()
        );
    }

    public function create($data)
    {
        $data['id'] = 0;

        return $this->update(0, $data);
    }

    public function update($id, $data)
    {
        $parcelTable = $this->container->get(ParcelTable::class);
        $tractorTable = $this->container->get(TractorTable::class);
        $form = new ParcelTreatmentForm(null, $parcelTable, $tractorTable);
        $parcelTreatment = new ParcelTreatment();

        $form->setInputFilter($parcelTreatment->getInputFilter($parcelTable));
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                'status' => 'error',
                'errors' => $form->getMessages(),
            ]);
        }

        $parcelTreatment->exchangeArray($form->getData());
        $this->table->saveParcelTreatment($parcelTreatment);

        return new JsonModel([
            'status' => 'ok',
        ]);
    }

    public function delete($id)
    {
        $this->table->deleteParcelTreatment($id);

        return new JsonModel([
            'status' => 'ok',
        ]);
    }
}
