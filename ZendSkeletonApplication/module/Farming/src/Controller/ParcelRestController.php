<?php

namespace Farming\Controller;

use Farming\Form\ParcelForm;
use Farming\Model\Parcel;
use Farming\Model\ParcelTable;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ParcelRestController extends AbstractRestfulController
{
    private $table;

    public function __construct(ParcelTable $table)
    {
        $this->table = $table;
    }

    public function getList()
    {
        return new JsonModel(
            $this->table->fetchAll()->toArray()
        );
    }

    public function get($id)
    {
        return new JsonModel(
            $this->table->getParcel($id)->getArrayCopy()
        );
    }

    public function create($data)
    {
        $data['id'] = 0;

        return $this->update(0, $data);
    }

    public function update($id, $data)
    {
        $form = new ParcelForm();
        $parcel = new Parcel();

        $form->setInputFilter($parcel->getInputFilter());
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                'status' => 'error',
                'errors' => $form->getMessages(),
            ]);
        }

        $parcel->exchangeArray($form->getData());
        $this->table->saveParcel($parcel);

        return new JsonModel([
            'status' => 'ok',
        ]);
    }

    public function delete($id)
    {
        $this->table->deleteParcel($id);

        return new JsonModel([
            'status' => 'ok',
        ]);
    }
}
