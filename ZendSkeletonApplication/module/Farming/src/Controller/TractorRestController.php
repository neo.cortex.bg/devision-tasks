<?php

namespace Farming\Controller;

use Farming\Form\TractorForm;
use Farming\Model\Tractor;
use Farming\Model\TractorTable;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class TractorRestController extends AbstractRestfulController
{
    private $table;

    public function __construct(TractorTable $table)
    {
        $this->table = $table;
    }

    public function getList()
    {
        return new JsonModel(
            $this->table->fetchAll()->toArray()
        );
    }

    public function get($id)
    {
        return new JsonModel(
            $this->table->getTractor($id)->getArrayCopy()
        );
    }

    public function create($data)
    {
        $data['id'] = 0;

        return $this->update(0, $data);
    }

    public function update($id, $data)
    {
        $form = new TractorForm();
        $tractor = new Tractor();

        $form->setInputFilter($tractor->getInputFilter());
        $form->setData($data);

        if (!$form->isValid()) {
            return new JsonModel([
                'status' => 'error',
                'errors' => $form->getMessages(),
            ]);
        }

        $tractor->exchangeArray($form->getData());
        $this->table->saveTractor($tractor);

        return new JsonModel([
            'status' => 'ok',
        ]);
    }

    public function delete($id)
    {
        $this->table->deleteTractor($id);

        return new JsonModel([
            'status' => 'ok',
        ]);
    }
}
