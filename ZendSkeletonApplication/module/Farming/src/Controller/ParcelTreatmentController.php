<?php

namespace Farming\Controller;

use Farming\Form\ParcelTreatmentFilterForm;
use Farming\Form\ParcelTreatmentForm;
use Farming\Model\ParcelTable;
use Farming\Model\ParcelTreatment;
use Farming\Model\ParcelTreatmentTable;
use Farming\Model\TractorTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\ViewModel;

class ParcelTreatmentController extends AbstractActionController
{
    private $table;
    private $container;

    public function __construct(ServiceManager $container, ParcelTreatmentTable $table)
    {
        $this->container = $container;
        $this->table = $table;
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $getParameters = $request->getQuery();

        $parcelTable = $this->container->get(ParcelTable::class);
        $tractorTable = $this->container->get(TractorTable::class);
        $filterForm = new ParcelTreatmentFilterForm(null, $parcelTable, $tractorTable);
        $filterForm->setData($getParameters);

        $filterForm->setAttribute('method', 'GET');

        return new ViewModel([
            'parcelTreatments' => $this->table->fetchAllWithParcel($getParameters),
            'form' => $filterForm,
        ]);
    }

    public function addAction()
    {
        $parcelTable = $this->container->get(ParcelTable::class);
        $tractorTable = $this->container->get(TractorTable::class);
        $form = new ParcelTreatmentForm(null, $parcelTable, $tractorTable);
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $parcelTreatment = new ParcelTreatment();
        $form->setInputFilter($parcelTreatment->getInputFilter($parcelTable));
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $parcelTreatment->exchangeArray($form->getData());
        $this->table->saveParcelTreatment($parcelTreatment);
        return $this->redirect()->toRoute('parcel_treatment');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('parcel_treatment', ['action' => 'add']);
        }

        // Retrieve the parcel with the specified id. Doing so raises
        // an exception if the parcel is not found, which should result
        // in redirecting to the landing page.
        try {
            $parcelTreatment = $this->table->getParcelTreatment($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('parcel_treatment', ['action' => 'index']);
        }

        $parcelTable = $this->container->get(ParcelTable::class);
        $tractorTable = $this->container->get(TractorTable::class);
        $form = new ParcelTreatmentForm(null, $parcelTable, $tractorTable);
        $form->bind($parcelTreatment);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($parcelTreatment->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->saveParcelTreatment($parcelTreatment);

        // Redirect to parcel list
        return $this->redirect()->toRoute('parcel_treatment', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('parcel_treatment');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteParcelTreatment($id);
            }

            // Redirect to list of parcels
            return $this->redirect()->toRoute('parcel');
        }

        return [
            'id'    => $id,
            'parcel' => $this->table->getParcelTreatment($id),
        ];
    }
}
