<?php

namespace Farming\Controller;

use Farming\Form\ParcelForm;
use Farming\Model\Parcel;
use Farming\Model\ParcelTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ParcelController extends AbstractActionController
{
    private $table;

    public function __construct(ParcelTable $table)
    {
        $this->table = $table;
    }

    public function indexAction()
    {
        return new ViewModel([
            'parcels' => $this->table->fetchAll(),
        ]);
    }

    public function addAction()
    {
        $form = new ParcelForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $parcel = new Parcel();
        $form->setInputFilter($parcel->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $parcel->exchangeArray($form->getData());
        $this->table->saveParcel($parcel);
        return $this->redirect()->toRoute('parcel');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('parcel', ['action' => 'add']);
        }

        // Retrieve the parcel with the specified id. Doing so raises
        // an exception if the parcel is not found, which should result
        // in redirecting to the landing page.
        try {
            $parcel = $this->table->getParcel($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('parcel', ['action' => 'index']);
        }

        $form = new ParcelForm();
        $form->bind($parcel);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($parcel->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->saveParcel($parcel);

        // Redirect to parcel list
        return $this->redirect()->toRoute('parcel', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('parcel');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteParcel($id);
            }

            // Redirect to list of parcels
            return $this->redirect()->toRoute('parcel');
        }

        return [
            'id'    => $id,
            'parcel' => $this->table->getParcel($id),
        ];
    }
}
