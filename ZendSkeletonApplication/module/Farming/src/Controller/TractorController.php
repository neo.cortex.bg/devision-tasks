<?php

namespace Farming\Controller;

use Farming\Form\TractorForm;
use Farming\Model\Tractor;
use Farming\Model\TractorTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TractorController extends AbstractActionController
{
    private $table;

    public function __construct(TractorTable $table)
    {
        $this->table = $table;
    }

    public function indexAction()
    {
        return new ViewModel([
            'tractors' => $this->table->fetchAll(),
        ]);
    }

    public function addAction()
    {
        $form = new TractorForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $tractor = new Tractor();
        $form->setInputFilter($tractor->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $tractor->exchangeArray($form->getData());
        $this->table->saveTractor($tractor);
        return $this->redirect()->toRoute('tractor');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('tractor', ['action' => 'add']);
        }

        // Retrieve the tractor with the specified id. Doing so raises
        // an exception if the tractor is not found, which should result
        // in redirecting to the landing page.
        try {
            $tractor = $this->table->getTractor($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('tractor', ['action' => 'index']);
        }

        $form = new TractorForm();
        $form->bind($tractor);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($tractor->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->saveTractor($tractor);

        // Redirect to tractor list
        return $this->redirect()->toRoute('tractor', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('tractor');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteTractor($id);
            }

            // Redirect to list of tractors
            return $this->redirect()->toRoute('tractor');
        }

        return [
            'id'    => $id,
            'tractor' => $this->table->getTractor($id),
        ];
    }
}
