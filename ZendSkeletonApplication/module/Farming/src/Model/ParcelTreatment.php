<?php

namespace Farming\Model;

use DomainException;
use Grpc\Call;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\Filter\DateSelect;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\StringLength;
use Zend\Validator\Callback;
use Zend\Db\Sql\Select;

class ParcelTreatment
{
    public $id;
    public $parcel_id;
    public $tractor_id;
    public $date;
    public $area;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->parcel_id = !empty($data['parcel_id']) ? $data['parcel_id'] : null;
        $this->culture = !empty($data['culture']) ? $data['culture'] : null;
        $this->tractor_id = !empty($data['tractor_id']) ? $data['tractor_id'] : null;
        $this->date = !empty($data['date']) ? $data['date'] : null;
        $this->area = !empty($data['area']) ? $data['area'] : null;
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'parcel_id' => $this->parcel_id,
            'culture' => $this->culture,
            'tractor_id' => $this->tractor_id,
            'date' => $this->date,
            'area' => $this->area,
        ];
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new DomainException(sprintf(
            '%s does not allow injection of an alternate input filter',
            __CLASS__
        ));
    }

    public function getInputFilter(ParcelTable $table)
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);
        $inputFilter->add([
            'name' => 'parcel_id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);
        $inputFilter->add([
            'name' => 'tractor_id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);
        $inputFilter->add([
            'name' => 'date',
            'required' => true,
            'filters' => [
                ['name' => DateSelect::class],
            ],
        ]);
        $inputFilter->add([
            'name' => 'area',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
            'validators' => [
                [
                    'name' => Callback::class,
                    'options' => [
                        'callback' => function ($value, $context) use ($table) {
                            return $table->getParcelWithBiggerArea($context['parcel_id'], $context['area']);
                        },
                    ],
                ],
            ],
        ]);

        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }
}
