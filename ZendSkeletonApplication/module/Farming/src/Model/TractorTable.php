<?php

namespace Farming\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

class TractorTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function getTractor($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveTractor(Tractor $tractor)
    {
        $data = [
            'name'  => $tractor->name,
        ];

        $id = (int) $tractor->id;

        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        if (! $this->getTractor($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update tractor with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteTractor($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}
