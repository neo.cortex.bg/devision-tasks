<?php

namespace Farming\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;

class ParcelTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function getParcelWithBiggerArea($id, $area)
    {
        $select = new Select('parcel');
        $select->where("id = " . (int)$id . " AND area > " . (int)$area . "");
        $res = $this->tableGateway->selectWith($select);

        return $res->current() !== null;
    }

    public function getParcel($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveParcel(Parcel $parcel)
    {
        $data = [
            'name'  => $parcel->name,
            'culture'  => $parcel->culture,
            'area'  => $parcel->area,
        ];

        $id = (int) $parcel->id;

        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        if (! $this->getParcel($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update parcel with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteParcel($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}
