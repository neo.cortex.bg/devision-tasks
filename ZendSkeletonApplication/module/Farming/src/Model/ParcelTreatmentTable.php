<?php

namespace Farming\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class ParcelTreatmentTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function fetchAllWithParcel($getParameters)
    {
        $params = [];
        foreach ($getParameters as $k => $v) {
            $params[$k] = trim($v);
        }
        foreach ($getParameters as $k => $v) {
            if(empty($v)) {
                unset($params[$k]);
            }
        }

        $select = new Select('parcel_treatment');
        $select->join('parcel', 'parcel_treatment.parcel_id = parcel.id', ['culture']);

        if (isset($params['parcel_id'])) {
            $select->where(['parcel_treatment.parcel_id' => (int)$params['parcel_id']]);
        }
        if (isset($params['tractor_id'])) {
            $select->where(['parcel_treatment.tractor_id' => (int)$params['tractor_id']]);
        }
        if (isset($params['date'])) {
            $select->where(['parcel_treatment.date' => $params['date']]);
        }

        if (isset($params['culture'])) {
            $culture = $params['culture'];
            $select->where(function (Where $where) use ($culture) {
                $where->like('parcel.culture', '%' . $culture . '%');
            });
        }

        return $this->tableGateway->selectWith($select);
    }

    public function getParcelTreatment($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveParcelTreatment(ParcelTreatment $parcelTreatment)
    {
        $data = [
            'id' => $parcelTreatment->id,
            'parcel_id' => $parcelTreatment->parcel_id,
            'tractor_id' => $parcelTreatment->tractor_id,
            'date' => $parcelTreatment->date,
            'area' => $parcelTreatment->area,
        ];

        $id = (int)$parcelTreatment->id;

        if ($id === 0) {
            $this->tableGateway->insert($data);

            return;
        }

        if (!$this->getParcelTreatment($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update parcel with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteParcelTreatment($id)
    {
        $this->tableGateway->delete(['id' => (int)$id]);
    }
}
