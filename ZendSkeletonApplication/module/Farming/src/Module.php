<?php

namespace Farming;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\ParcelTable::class => function($container) {
                    $tableGateway = $container->get(Model\ParcelTableGateway::class);
                    return new Model\ParcelTable($tableGateway);
                },
                Model\ParcelTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Parcel());
                    return new TableGateway('parcel', $dbAdapter, null, $resultSetPrototype);
                },
                Model\TractorTable::class => function($container) {
                    $tableGateway = $container->get(Model\TractorTableGateway::class);
                    return new Model\TractorTable($tableGateway);
                },
                Model\TractorTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Tractor());
                    return new TableGateway('tractor', $dbAdapter, null, $resultSetPrototype);
                },
                Model\ParcelTreatmentTable::class => function($container) {
                    $tableGateway = $container->get(Model\ParcelTreatmentTableGateway::class);
                    return new Model\ParcelTreatmentTable($tableGateway);
                },
                Model\ParcelTreatmentTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ParcelTreatment());
                    return new TableGateway('parcel_treatment', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\ParcelController::class => function($container) {
                    return new Controller\ParcelController(
                        $container->get(Model\ParcelTable::class)
                    );
                },
                Controller\ParcelTreatmentController::class => function($container) {
                    return new Controller\ParcelTreatmentController(
                        $container,
                        $container->get(Model\ParcelTreatmentTable::class)
                    );
                },
                Controller\TractorController::class => function($container) {
                    return new Controller\TractorController(
                        $container->get(Model\TractorTable::class)
                    );
                },

                // Api Controllers
                Controller\ParcelRestController::class => function($container) {
                    return new Controller\ParcelRestController(
                        $container->get(Model\ParcelTable::class)
                    );
                },
                Controller\ParcelTreatmentRestController::class => function($container) {
                    return new Controller\ParcelTreatmentRestController(
                        $container,
                        $container->get(Model\ParcelTreatmentTable::class)
                    );
                },
                Controller\TractorRestController::class => function($container) {
                    return new Controller\TractorRestController(
                        $container->get(Model\TractorTable::class)
                    );
                },
            ],
        ];
    }
}
