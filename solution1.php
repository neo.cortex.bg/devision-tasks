<?php

function solution(array $a)
{
    $lastDifference = null;
    $count = count($a);

    for ($i = 1; $i < $count; $i++) {

        $a1 = array_slice($a, 0, $i);
        $a2 = array_slice($a, $i);

        $sum1 = array_sum($a1);
        $sum2 = array_sum($a2);

        $currentDifference = abs($sum1 - $sum2);

        if ($i === 1) {
            $lastDifference = $currentDifference;
        }

        if ($currentDifference < $lastDifference) {
            $lastDifference = $currentDifference;
        }
    }

    return $lastDifference;
}
